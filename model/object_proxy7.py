# -*- coding: utf-8 -*-
# This file is part of OpenERP. The COPYRIGHT file at the top level of
# this module contains the full copyright notices and license terms.

from openerp.osv import fields, orm
from openerp.osv.osv import object_proxy
from openerp import SUPERUSER_ID
from openerp import pooler


def get_unloggable_methods():
    """Returns ORM methods which is not logged when log_action is set to True"""
    
    return ['default_get', 'fields_view_get', 'fields_get', 'search', 'search_count',
            'name_search','name_get','get','request_get', 'get_sc', 'read_group',
            'import_data', 'message_subscription_data',
            'message_get_subscription_data']


class audittrail7_objects_proxy(object_proxy):
    """ Uses Object proxy for auditing changes on object of subscribed Rules"""

    def _get_field_id_name(self, cr, uid, model_name, field_name):
        """Returns provided field's id and name"""
        pool = pooler.get_pool(cr.dbname)
        obj_pool = pool.get(model_name)
        model_pool = pool.get('ir.model')
        field_pool = pool.get('ir.model.fields')
        
        model_ids = pool.get('ir.model').search(cr, SUPERUSER_ID, [('model', '=', model_name)])
        field_obj = obj_pool._all_columns.get(field_name)
        assert field_obj, _("'%s' field does not exist in '%s' model" %(field_name, model_name))
        field_obj = field_obj.column
        search_models = [model_ids]
        if obj_pool._inherits:
            search_models += model_pool.search(cr, SUPERUSER_ID, [('model', 'in', obj_pool._inherits.keys())])
        field_ids = field_pool.search(cr, SUPERUSER_ID, [('name', '=', field_name), ('model_id', 'in', search_models)])
        field_id = field_ids and field_ids[0] or False,
        return field_id[0], field_obj.string

    def _get_value_text(self, cr, uid, pool, model_pool, field, value):
        """Returns textual representation of provided value
        
        E.g. value is relational field (1, 'Your Compnay') then it returns
        'Your Company'"""
        field_obj = (model_pool._all_columns.get(field)).column
        if field_obj._type in ('one2many','many2many'):
            data = pool.get(field_obj._obj).name_get(cr, SUPERUSER_ID, value)
            #return the modifications on x2many fields as a list of names
            res = map(lambda x:x[1], data)
            res = '; '.join(res)
        elif field_obj._type == 'many2one':
            #return the modifications on a many2one field as its value returned by name_get()
            res = value and value[1] or value
        elif field_obj._type == 'boolean':
            res = value and 'True'
        elif field_obj._type == 'binary':
            res = '<binary_data>'
        else:
            res = value
        return res

    def _create_log(self, cr, uid, model, method, res_id):
        """Creates audittrail7.log object and returns it's ID"""
        if type(res_id) != list:
            res_id = [res_id]
        pool = pooler.get_pool(cr.dbname)
        name = pool.get(model).name_get(cr, uid, res_id)[0][1]
        model_ids = pool.get('ir.model').search(cr, SUPERUSER_ID, [('model', '=', model)])
        model_id = model_ids and model_ids[0] or False
        vals = {
            'method': method,
            'object_id': model_id,
            'user_id': uid,
            'res_id': res_id[0],
            'name': name,
        }
        return pool.get('audittrail7.log').create(cr, SUPERUSER_ID, vals)

    def log_read(self, cr, uid, model, method, fct_src, *args, **kw):
        """Logging function for logs on orm method 'read'
        There are no way to distinguish when an object was opened using form and
        when tree view, so we have to make really stupid assumptions
        """
        res_id = args and args[0] or False
        # assumption #1: when we open in form view we have only 1 object id
        if res_id and len(res_id) == 1:
            # assumption #2: we have to pass the threshold defined on audittrail rule
            # for that model in order filter associated objects with x2many relation
            pool = pooler.get_pool(cr.dbname)
            rule_pool = pool.get('audittrail7.rule')
            rule_ids = rule_pool.search(cr, SUPERUSER_ID, [('object_id', '=', model),
                                                           ('state', '=', 'subscribed')])
            rule_id = rule_ids and rule_ids[0] or False
            if rule_id:
                read_threshold = rule_pool.read(cr, SUPERUSER_ID, rule_id, ['log_read_threshold'])['log_read_threshold']
                if len(args[1]) > read_threshold:
                    self._create_log(cr, uid, model, method, res_id)
        return fct_src(cr, uid, model, method, *args, **kw)
    
    def log_create(self, cr, uid, model, method, fct_src, *args, **kw):
        to_ret = fct_src(cr, uid, model, method, *args, **kw)
        pool = pooler.get_pool(cr.dbname)
        model_pool = pool.get(model)
        line_data = []
        for res in model_pool.read(cr, SUPERUSER_ID, [to_ret]):
            log_id = self._create_log(cr, uid, model, method, res['id'])
            for field in res:
                if field in ('__last_update', 'id'):
                    continue
                field_id, field_name = self._get_field_id_name(cr, uid, model, field)
                val_text = self._get_value_text(cr, uid, pool, model_pool, field, res[field])
                line_data.append({
                    'log_id': log_id,
                    'field_id': field_id,
                    'new_value': res[field],
                    'new_value_text': val_text or '',
                    'field_description': field_name,
                    })
            if line_data:
                log_line_pool = pool.get('audittrail7.log.line')
                [log_line_pool.create(cr, SUPERUSER_ID, vals) for vals in line_data if vals['new_value_text']]
        return to_ret

    def log_name_create(self, cr, uid, model, method, fct_src, *args, **kw):
        to_ret = fct_src(cr, uid, model, method, *args, **kw)
        res_id = to_ret and to_ret[0] or False
        if res_id:
            log_id = self._create_log(cr, uid, model, 'create', res_id)
        return to_ret

    def log_message_post(self, cr, uid, model, method, fct_src, *args, **kw):
        to_ret = fct_src(cr, uid, model, method, *args, **kw)
        res_id = args and args[0] or False
        if res_id:
            pool = pooler.get_pool(cr.dbname)
            log_id = self._create_log(cr, uid, model, method, res_id)
            log_line_pool = pool.get('audittrail7.log.line')
            field_id, field_name = self._get_field_id_name(cr, uid, 'mail.message', 'body')
            line_vals = {
                    'log_id': log_id,
                    'field_id': field_id,
                    'new_value': kw.get('body', ''),
                    'new_value_text': kw.get('body', ''),
                    'field_description': field_name,
                    }
            log_line_pool.create(cr, SUPERUSER_ID, line_vals)
        return to_ret

    def log_write(self, cr, uid, model, method, fct_src, *args, **kw):
        changed_fields = {}
        field_value = args and args[1] or False
        if field_value:
            pool = pooler.get_pool(cr.dbname)
            model_pool = pool.get(model)
            log_line_pool = pool.get('audittrail7.log.line')
            old_vals = model_pool.read(cr, uid, args[0], field_value.keys())[0]
            to_ret = fct_src(cr, uid, model, method, *args, **kw)
            new_vals = model_pool.read(cr, uid, args[0], field_value.keys())[0]
            log_id = self._create_log(cr, uid, model, method, args[0])
            for field, new_val in field_value.iteritems():
                field_id, field_name = self._get_field_id_name(cr, uid, model, field)
                new_val_text = self._get_value_text(cr, uid, pool, model_pool, field, new_vals[field])
                old_val_text = self._get_value_text(cr, uid, pool, model_pool, field, old_vals[field])
                vals = {
                    'log_id': log_id,
                    'field_id': field_id,
                    'old_value': old_vals[field],
                    'new_value': new_vals[field],
                    'old_value_text': old_val_text or '',
                    'new_value_text': new_val_text or '',
                    'field_description': field_name,
                    }
                log_line_pool.create(cr, SUPERUSER_ID, vals)
            return to_ret
        return fct_src(cr, uid, model, method, *args, **kw)

    def log_unlink(self, cr, uid, model, method, fct_src, *args, **kw):
        pool = pooler.get_pool(cr.dbname)
        model_pool = pool.get(model)
        res_id = args and args[0] or False
        if res_id:
            log_id = self._create_log(cr, uid, model, method, res_id)
        return fct_src(cr, uid, model, method, *args, **kw)

    def log_action(self, cr, uid, model, method, fct_src, *args, **kw):
        pool = pooler.get_pool(cr.dbname)
        model_pool = pool.get(model)
        if args:
            res_ids = args[0]
            if isinstance(res_ids, (long, int)):
                res_ids = [res_ids]
            if res_ids:
                log_id = self._create_log(cr, uid, model, method, res_ids)
        return fct_src(cr, uid, model, method, *args, **kw)

    def get_logger_function(self, cr, uid, model, method):
        """
        Checks if audit7 rule exists on provided *model* for provided *method*
        and returns it's handler or False otherwise.
        """
        pool = pooler.get_pool(cr.dbname)
        if 'audittrail7.rule' in pool.models:
            model_ids = pool.get('ir.model').search(cr, SUPERUSER_ID, [('model', '=', model)])
            model_id = model_ids and model_ids[0] or False
            if model_id:
                rule_pool = pool.get('audittrail7.rule')
                rule_ids = rule_pool.search(cr, SUPERUSER_ID, [('object_id', '=', model_id), ('state', '=', 'subscribed')])
                for rule in rule_pool.read(cr, SUPERUSER_ID, rule_ids, 
                                           ['user_id','log_read','log_write',
                                            'log_create','log_unlink','log_action',
                                            'log_workflow', 'log_message_post']):
                    if len(rule['user_id']) == 0 or uid in rule['user_id']:
                        logger_name = rule.get('log_'+method, False)
                        if logger_name:
                            return getattr(self, 'log_'+method)
                        #special case when using quick creation
                        elif method == 'name_create' and rule.get('log_create', False):
                            return getattr(self, 'log_'+method)
                        elif rule.get('log_action', False):
                            # skip standart methods
                            if method in get_unloggable_methods():
                                pass
                            else:
                                try:
                                    # skip implemented functions (e.g. write, read etc.)
                                    getattr(self, 'log_'+method)
                                    return False
                                except AttributeError:
                                    pass
                                return getattr(self, 'log_action')
        return False

    def execute_cr(self, cr, uid, model, method, *args, **kw):
        fct_src = super(audittrail7_objects_proxy, self).execute_cr
        logger = self.get_logger_function(cr, uid, model, method)
        if logger:
            return logger(cr, uid, model, method, fct_src, *args, **kw)
        return fct_src(cr, uid, model, method, *args, **kw)

    def exec_workflow_cr(self, cr, uid, model, method, *args, **kw):
        fct_src = super(audittrail7_objects_proxy, self).exec_workflow_cr
        return fct_src(cr, uid, model, method, *args, **kw)

audittrail7_objects_proxy()
