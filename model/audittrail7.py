# -*- coding: utf-8 -*-
# This file is part of OpenERP. The COPYRIGHT file at the top level of
# this module contains the full copyright notices and license terms.

from openerp.osv import fields, orm
from openerp import SUPERUSER_ID
import time
import datetime # TODO: discard this dependency in a future

class audittrail7_rule(orm.Model):
    """
    For Auddittrail7 Rule
    """
    _name = 'audittrail7.rule'
    _description = "Audittrail7 Rule"
    _columns = {
        "name": fields.char("Rule Name", size=32, required=True),
        "object_id": fields.many2one('ir.model', 'Object', required=True, help="Select object for which you want to generate log."),
        "user_id": fields.many2many('res.users', 'audittail_rules_users',
                                            'user_id', 'rule_id', 'Users', help="if  User is not added then it will applicable for all users"),
        "log_read": fields.boolean("Log Reads", help="Select this if you want to keep track of read/open on any record of the object of this rule"),
        "log_write": fields.boolean("Log Writes", help="Select this if you want to keep track of modification on any record of the object of this rule"),
        "log_unlink": fields.boolean("Log Deletes", help="Select this if you want to keep track of deletion on any record of the object of this rule"),
        "log_create": fields.boolean("Log Creates",help="Select this if you want to keep track of creation on any record of the object of this rule. "
                                     "Be aware that objects aren't logged if created from one2many field. E.g. Invoice Line won't be logged when created "
                                     "from Invoice form."),
        "log_action": fields.boolean("Log Action",help="Select this if you want to keep track of actions on the object of this rule"),
        "log_message_post": fields.boolean("Log Message Posting",help="Select this if you want to keep track of posted messages on object"),
        "log_workflow": fields.boolean("Log Workflow", readonly=True, # TODO: Implement me
                                       help="Not implemented! Select this if you want to keep track of workflow on any record of the object of this rule"),
        "log_read_threshold": fields.integer("Read Threshold"),
        "state": fields.selection((("draft", "Draft"), ("subscribed", "Subscribed")), "Status", required=True),
        "action_id": fields.many2one('ir.actions.act_window', "Action ID"),
    }
    _defaults = {
        'state': 'draft',
        'log_create': 1,
        'log_unlink': 1,
        'log_write': 1,
    }
    _sql_constraints = [
        ('model_uniq', 'unique (object_id)', """There is already a rule defined on this object\n You cannot define another: please edit the existing one.""")
    ]
    __functions = {}

    def subscribe(self, cr, uid, ids, *args):
        """
        Subscribe Rule for auditing changes on object and apply shortcut for logs on that object.
        @param cr: the current row, from the database cursor,
        @param uid: the current user’s ID for security checks,
        @param ids: List of Auddittrail Rule’s IDs.
        @return: True
        """
        obj_action = self.pool.get('ir.actions.act_window')
        obj_model = self.pool.get('ir.model.data')
        #start Loop
        for thisrule in self.browse(cr, uid, ids):
            obj = self.pool.get(thisrule.object_id.model)
            if not obj:
                raise osv.except_osv(
                        _('WARNING: audittrail is not part of the pool'),
                        _('Change audittrail depends -- Setting rule as DRAFT'))
                self.write(cr, uid, [thisrule.id], {"state": "draft"})
            val = {
                 "name": 'View Log',
                 "res_model": 'audittrail7.log',
                 "src_model": thisrule.object_id.model,
                 "domain": "[('object_id','=', " + str(thisrule.object_id.id) + "), ('res_id', '=', active_id)]"

            }
            action_id = obj_action.create(cr, SUPERUSER_ID, val)
            self.write(cr, uid, [thisrule.id], {"state": "subscribed", "action_id": action_id})
            keyword = 'client_action_relate'
            value = 'ir.actions.act_window,' + str(action_id)
            res = obj_model.ir_set(cr, SUPERUSER_ID, 'action', keyword, 'View_log_' + thisrule.object_id.model, [thisrule.object_id.model], value, replace=True, isobject=True, xml_id=False)
            #End Loop
        return True

    def unsubscribe(self, cr, uid, ids, *args):
        """
        Unsubscribe Auditing Rule on object
        @param cr: the current row, from the database cursor,
        @param uid: the current user’s ID for security checks,
        @param ids: List of Auddittrail Rule’s IDs.
        @return: True
        """
        obj_action = self.pool.get('ir.actions.act_window')
        ir_values_obj = self.pool.get('ir.values')
        value=''
        #start Loop
        for thisrule in self.browse(cr, uid, ids):
            if thisrule.id in self.__functions:
                for function in self.__functions[thisrule.id]:
                    setattr(function[0], function[1], function[2])
            w_id = obj_action.search(cr, uid, [('name', '=', 'View Log'), ('res_model', '=', 'audittrail7.log'), ('src_model', '=', thisrule.object_id.model)])
            if w_id:
                obj_action.unlink(cr, SUPERUSER_ID, w_id)
                value = "ir.actions.act_window" + ',' + str(w_id[0])
            val_id = ir_values_obj.search(cr, uid, [('model', '=', thisrule.object_id.model), ('value', '=', value)])
            if val_id:
                ir_values_obj = pooler.get_pool(cr.dbname).get('ir.values')
                res = ir_values_obj.unlink(cr, uid, [val_id[0]])
            self.write(cr, uid, [thisrule.id], {"state": "draft"})
        #End Loop
        return True

class audittrail7_log(orm.Model):
    """
    For Audittrail Log
    """
    _name = 'audittrail7.log'
    _description = "Audittrail7 Log"

    def _name_get_resname(self, cr, uid, ids, *args):
        data = {}
        for resname in self.browse(cr, uid, ids,[]):
            model_object = resname.object_id
            res_id = resname.res_id
            if model_object and res_id:
                model_pool = self.pool.get(model_object.model)
                res = model_pool.read(cr, uid, res_id, ['name'])
                data[resname.id] = res['name']
            else:
                 data[resname.id] = False
        return data

    
    def _get_ref(self, cr, uid, context=None):
        obj = self.pool.get('ir.model')
        ids = obj.search(cr, uid, [])
        res = obj.read(cr, uid, ids, ['model', 'name'], context=context)
        return [(r['model'], r['name']) for r in res]
    
    def _day(self, cr, uid, ids, name="", args={}, context=None):
        result = {}
        for t in self.browse(cr, uid, ids, context=context):
            result[t.id] = t.timestamp[0:10]

        return result

    def _week(self, cr, uid, ids, name, arg, context=None):
        result = {}
        for t in self.browse(cr, uid, ids, context=context):
            dt = datetime.date(int(t.timestamp[0:4]), int(t.timestamp[6]), int(t.timestamp[8:10]))
            wk = dt.isocalendar()[1]
            result[t.id] = t.timestamp[0:4] + ' week ' + str(wk)

        return result


    _columns = {
        "name": fields.char("Resource Name",size=64),
        "object_id": fields.many2one('ir.model', 'Object'),
        "user_id": fields.many2one('res.users', 'User'),
        "method": fields.char("Method", size=64),
        "timestamp": fields.datetime("Date"),
        "res_id": fields.integer('Resource Id'),
        "reference_id": fields.reference('Resource', selection=_get_ref, size=256),
        "line_ids": fields.one2many('audittrail7.log.line', 'log_id', 'Log lines'),
        'day': fields.function(_day, type='char', string="Day", store=True),
        'week': fields.function(_week, type='char', string="Week", store=True),
    }

    _defaults = {
        "timestamp": lambda *a: time.strftime("%Y-%m-%d %H:%M:%S")
    }
    _order = "timestamp desc"

class audittrail7_log_line(orm.Model):
    """
    Audittrail7 Log Line.
    """
    _name = 'audittrail7.log.line'
    _description = "Log Line"
    _columns = {
          'field_id': fields.many2one('ir.model.fields', 'Fields', required=True),
          'log_id': fields.many2one('audittrail7.log', 'Log'),
          'log': fields.integer("Log ID"),
          'old_value': fields.text("Old Value"),
          'new_value': fields.text("New Value"),
          'old_value_text': fields.text('Old value Text'),
          'new_value_text': fields.text('New value Text'),
          'field_description': fields.char('Field Description', size=64),
        }
