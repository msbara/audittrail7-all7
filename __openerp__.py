# -*- coding: utf-8 -*-
# This file is part of OpenERP. The COPYRIGHT file at the top level of
# this module contains the full copyright notices and license terms.

{
    'name' : 'Audittrail 7 - all7',
    'version': '0.1',
    'author': 'HacBee UAB',
    'category': 'Tools',
    'website': 'http://www.hacbee.com',
    'summary': 'Audittrail module for v7',
    'description': """
A fork from SYSBIN to make it installable along with audittrail from OpenERP SA
This is a working(!) version of audittrail module designed for OpenERP v7
===============================================================================

Main differences from standard audittrail module:
----------------------------------------------------------
* Logs 'reads' only when an object is opened in form view (used to be on all reads which produced immense amount of data when e.g. opening a list view). You need to specify a threshold on how many fields are read in order to assume that it's a form reading.
* Fixed unlink method logging
* Added message_post logging which is used when leaving a note on an object
* Better grouping/filtering options on log objects.
Unfortunately, there is one bug: when objects are filtered to one and read threshold is not high enough the log could be created in list view.
""",
    'depends': [
        'base',
    ],
    'data': ['view/audittrail7.xml'],
    'update_xml': [],
    'installable': True,
    'application': False,
}
